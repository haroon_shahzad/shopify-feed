import { getSessionToken } from "@shopify/app-bridge-utils";
import { useAppBridge } from "@shopify/app-bridge-react";

import axios from "axios";

export const useAxios = () => {
  const service = axios.create();
  const app = useAppBridge();
  service.interceptors.request.use((config) => {
    return getSessionToken(app).then((token) => {
      config.headers["Authorization"] = `Bearer ${token}`;
      return config;
    });
  });

  return [service];
};
