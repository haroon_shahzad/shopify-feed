import axios from "axios";
import Shopify from "@shopify/shopify-api";

export async function makeRequest(ctx) {
  const session = await Shopify.Utils.loadCurrentSession(ctx.req, ctx.res);
  const { shop, accessToken } = session;
  const baseURL = `https://${shop}/admin/api/2021-07`;

  const service = axios.create({
    baseURL,
  });

  service.interceptors.request.use(
    (config) => {
      config.headers["Content-Type"] = "application/json";
      config.headers["X-Shopify-Access-Token"] = accessToken;

      return config;
    },
    (error) => {
      console.log("ERROR from axios request");
      return Promise.reject(error);
    }
  );

  return { axios: service, shop, accessToken };
}
