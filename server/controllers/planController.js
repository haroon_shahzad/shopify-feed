import Plan from "../model/Plan";
import Shop from "../model/Shop";
import Subscription from "../model/Subscriptions";

import { makeRequest } from "../axios/axios";
import { handleRequest } from "../app-init";

export const fetchAllPlans = async (ctx) => {
  try {
    const plans = await Plan.find();

    // /admin/api/2021-04/recurring_application_charges/{recurring_application_charge_id}.json

    ctx.body = plans;
    handleRequest(ctx);
  } catch (error) {
    console.log(error);
  }
};

export const makeSubscription = async (ctx) => {
  try {
    const { plan_id } = ctx.request.body;
    const { axios, shop } = await makeRequest(ctx);

    const plan = await Plan.findById(plan_id);

    if (!plan) throw new Error("Plan not Found");

    const resp = await axios.post("/recurring_application_charges.json", {
      recurring_application_charge: {
        name: plan.name,
        price: plan.price,
        return_url: `http://${shop}/admin/apps/fb-feed/subscription-confirmation?shop=${shop}`,
        test: true,
      },
    });

    const subscriptionData = resp.data.recurring_application_charge;
    ctx.body = subscriptionData;
    const { confirmation_url } = subscriptionData;

    // plan id will fetch from post body request

    const availableShop = await Shop.findOne({ shop });
    if (!shop_id) throw new Error("Shop not found...!");

    availableShop.isSubscribed = true;
    availableShop.plan_id = plan_id;
    availableShop.shopify_charge_id = subscriptionData.id;
    availableShop.subscription_details = subscriptionData;
    await availableShop.save();

    ctx.redirect(confirmation_url);
  } catch (error) {
    console.log(error);
  }
};

export const subscriptionConfirmation = async (ctx) => {
  try {
    // fetch the shop name from query param

    let shop = ctx.query.shop;

    console.log({ shop });

    const availableShop = await Shop.findOne({ shop });
    if (!availableShop) {
      throw new Error("Shop not Available");
    }

    const shopify_charge_id = availableShop.shopify_charge_id;
    const url = `/recurring_application_charges/${shopify_charge_id}.json`;
    const { axios } = await makeRequest();
    const resp = await axios.get(url);
    const subscription_details = resp.data.recurring_application_charge;
    availableShop.subscription_details = subscription_details;
    await available.save();

    ctx.redirect("/");
    handleRequest(ctx);
  } catch (error) {}
};
