import fs from "fs";
import RSS from "rss";
import JSZip from "jszip";

import { makeRequest } from "../axios/axios";
import { handleRequest } from "../app-init";

export const getProductList = async (ctx, next) => {
  try {
    const { axios } = await makeRequest(ctx);
    const resp = await axios.get("products.json");
    ctx.body = resp.data;
    handleRequest(ctx);
  } catch (error) {
    console.log("SERVER ERROR", error);
  }
};

export const createProductFeed = async (ctx, next) => {
  try {
    const { axios, shop } = await makeRequest(ctx);
    const resp = await axios.get("products.json");

    const products = resp.data.products;

    const feed = new RSS({
      title: shop,
      description: "this is the description",
      generator: "facebook feed generator",

      custom_namespaces: {
        g: "http://www.itunes.com/dtds/podcast-1.0.dtd",
      },
      // custom_elements: [{ link: "http://example.com/icon.png" }],
    });

    products.forEach((product) => {
      feed.item({
        title: "item title",
        description: "use this for the content. It can include html.",
        custom_elements: [
          { "g:item_group_id": "SKU-123123" },
          { "g:gtin": "A short primer on table spices" },
          {
            "g:google_product_category": product.tags,
          },
          { "g:id": "SKU-123123-GREEN" },
          { "g:title": product.title },
          {
            "g:description":
              "This product is the product you need to do the thing",
          },
          { "g:link": "https://www.mydealsshop.foo/products/widgetthing<" },
          { "g:image_link": "https://cdn.mycdn.foo/files/123123123.jpg<" },
          {
            "g:additional_image_link":
              "https://cdn.mycdn.foo/files/123123123.jpg",
          },
          {
            "g:additional_image_link":
              "https://cdn.mycdn.foo/files/123123123.jpg",
          },
          { color: "Green" },
          {
            additional_variant_attribute: [
              { label: "Style" },
              { value: "Cool" },
            ],
          },
          { "g:brand": "AcmeCo" },
          { "g:condition": "New" },
          { "g:availability": "in stock" },
          { "g:price": "19.99 USD" },
          { "g:sale_price": "19.99 USD" },
        ],
      });
    });

    const xml = feed.xml({ indent: true });

    const zip = new JSZip();
    zip.file(`${shop}.xml`, xml);

    const content = await zip.generateAsync({ type: "nodebuffer" });
    // const feedFolderPath = `/feed/${shop}.zip`;
    const feedFolderPath = `/feed/${shop}.gz`;

    fs.writeFileSync(`.${feedFolderPath}`, content);
    const pathToFeed = `${process.env.HOST}${feedFolderPath}`;
    ctx.body = pathToFeed;
    handleRequest(ctx);
  } catch (error) {
    console.log("SERVER ERROR", error);
  }
};
