import mongoose from "mongoose";

const shopSchema = new mongoose.Schema(
  {
    shop: {
      type: String,
      required: true,
      trim: true,
    },
    isSubscribed: {
      type: Boolean,
      default: false,
    },
    accessToken: {
      type: String,
      required: true,
      trim: true,
    },
    scope: {
      type: String,
      required: false,
    },
    plan_id: {
      type: mongoose.Types.ObjectId,
      ref: "Plan",
    },
    shopify_charge_id: {
      type: String,
      default: "",
    },
    subscription_details: {
      type: Map,
    },
  },
  {
    timestamps: true,
  }
);

const Shop = mongoose.model("Shop", shopSchema);

export default Shop;
