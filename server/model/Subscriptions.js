import mongoose from "mongoose";

const subscriptionSchema = new mongoose.Schema(
  {
    shop: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Shop",
    },
    subscriptionId: {
      type: String,
      required: true,
    },
    details: {
      type: Map,
    },
  },
  {
    timestamps: true,
  }
);

const Subscription = mongoose.model("Subscription", subscriptionSchema);

export default Subscription;
