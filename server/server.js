import dotenv from "dotenv";
import "@babel/polyfill";
import "isomorphic-fetch";
import createShopifyAuth, { verifyRequest } from "@shopify/koa-shopify-auth";
import Shopify, { ApiVersion } from "@shopify/shopify-api";
import Koa from "koa";
import Router from "koa-router";
var bodyParser = require("koa-bodyparser");

// initialize db
import "./db/index";

import app, { handleRequest } from "./app-init";
import Shop from "./model/Shop";
import { getProductList, createProductFeed } from "./controllers/index";
import {
  fetchAllPlans,
  makeSubscription,
  subscriptionConfirmation,
} from "./controllers/planController";

dotenv.config();
const port = parseInt(process.env.PORT, 10) || 8081;

import {
  storeCallback,
  loadCallback,
  deleteCallback,
} from "./session-handler/SessionHandler";

Shopify.Context.initialize({
  API_KEY: process.env.SHOPIFY_API_KEY,
  API_SECRET_KEY: process.env.SHOPIFY_API_SECRET,
  SCOPES: process.env.SCOPES.split(","),
  HOST_NAME: process.env.HOST.replace(/https:\/\//, ""),
  API_VERSION: ApiVersion.October20,
  IS_EMBEDDED_APP: true,
  // This should be replaced with your preferred storage strategy
  SESSION_STORAGE: new Shopify.Session.CustomSessionStorage(
    storeCallback,
    loadCallback,
    deleteCallback
  ),
});

// Storing the currently active shops in memory will force them to re-login when your server restarts. You should
// persist this object in your app.
const ACTIVE_SHOPIFY_SHOPS = {};

app.prepare().then(async () => {
  const server = new Koa();
  const router = new Router();
  server.use(bodyParser());

  server.keys = [Shopify.Context.API_SECRET_KEY];
  server.use(
    createShopifyAuth({
      async afterAuth(ctx) {
        // Access token and shop available in ctx.state.shopify
        const { shop, accessToken, scope } = ctx.state.shopify;
        const host = ctx.query.host;
        ACTIVE_SHOPIFY_SHOPS[shop] = scope;

        const shopExist = await Shop.findOne({ shop });

        if (!shopExist) {
          // create a new shop if not exist
          const newShop = await new Shop({
            shop,
            accessToken,
            scope,
          });
          await newShop.save();
        } else {
          // check if the token changed
          if (accessToken !== shopExist.accessToken) {
            shopExist.accessToken = accessToken;
            await shopExist.save();
          }
        }

        const response = await Shopify.Webhooks.Registry.register({
          shop,
          accessToken,
          path: "/webhooks",
          topic: "APP_UNINSTALLED",
          webhookHandler: async (topic, shop, body) => {
            // removing the shop from the DB
            const shopExist = await Shop.findOne({ shop });
            if (shopExist) {
              shopExist.remove();
            }
          },
        });

        if (!response.success) {
          console.log(
            `Failed to register APP_UNINSTALLED webhook: ${response.result}`
          );
        }

        // Redirect to app with shop parameter upon auth
        ctx.redirect(`/?shop=${shop}&host=${host}`);
      },
    })
  );

  router.post("/webhooks", async (ctx) => {
    try {
      await Shopify.Webhooks.Registry.process(ctx.req, ctx.res);
      console.log(`Webhook processed, returned status code 200`);
    } catch (error) {
      console.log(`Failed to process webhook: ${error}`);
    }
  });

  router.post(
    "/graphql",
    verifyRequest({ returnHeader: true }),
    async (ctx, next) => {
      await Shopify.Utils.graphqlProxy(ctx.req, ctx.res);
    }
  );

  router.get("/get-products", verifyRequest(), getProductList);
  router.get("/create-feed", verifyRequest(), createProductFeed);

  // subscription related routes
  router.post("/make-subscription", verifyRequest(), makeSubscription);

  router.get("/subscription-confirmation", subscriptionConfirmation);

  // plans related
  router.get("/fetch-all-plans", verifyRequest(), fetchAllPlans);

  router.get("(/_next/static/.*)", handleRequest); // Static content is clear
  router.get("/_next/webpack-hmr", handleRequest); // Webpack content is clear
  router.get("(.*)", async (ctx) => {
    const shop = ctx.query.shop;

    // This shop hasn't been seen yet, go through OAuth to create a session
    if (ACTIVE_SHOPIFY_SHOPS[shop] === undefined) {
      ctx.redirect(`/auth?shop=${shop}`);
    } else {
      await handleRequest(ctx);
    }
  });

  server.use(router.allowedMethods());
  server.use(router.routes());
  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});
