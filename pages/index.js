import {
  Heading,
  Page,
  DisplayText,
  List,
  Card,
  DataTable,
  Button,
} from "@shopify/polaris";
import { useEffect, useState } from "react";
import { useAxios } from "../hooks/useAxios";
import Home from "../components/Home";

const Index = () => {
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [plans, setPlans] = useState([]);
  const [axios] = useAxios();

  const fetchProducts = async () => {
    try {
      const resp = await axios.get("/get-products");
      setLoading(false);
      setProducts(resp.data.products);
    } catch (error) {
      console.log(error);
    }
  };

  const makeFeedHandler = async () => {
    try {
      setLoading(true);
      const resp = await axios.get("/create-feed");
      console.log("resp ", resp);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  const fetchPlans = async () => {
    try {
      setLoading(true);
      const resp = await axios.get("/fetch-all-plans");
      console.log("resp ", resp);
      setPlans(resp.data);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    // fetchProducts();
    // makeFeedHandler();
    fetchPlans();
  }, []);

  const planSubscriptionHandler = async (plan_id) => {
    try {
      setLoading(true);
      console.log("Plan ID", plan_id);
      const resp = await axios.post("/make-subscription", { plan_id });
      //
      console.log(resp);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  let productList = products.map((prod) => (
    <List.Item key={prod.id}>{prod.title}</List.Item>
  ));

  const rows = plans.map((plan) => {
    const { _id, name, price } = plan;
    // return [_id, name, price, <Button>Subscribe</Button>];

    return (
      <tr key={_id}>
        <td> {_id} </td>
        <td>{name}</td>
        <td>{price}</td>
        <td>
          <Button onClick={() => planSubscriptionHandler(_id)}>
            Subscribe
          </Button>
        </td>
      </tr>
    );
  });

  return (
    <div className="App">
      <Home />
    </div>
  );
};

export default Index;
