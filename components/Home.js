import BottomSection from "./BottomSection";
import ProductFeed from "./ProductFeed";
import Header from "./Header";

const Home = () => {
  return (
    <>
      <Header />
      <ProductFeed />
      <BottomSection />
    </>
  );
};

export default Home;
