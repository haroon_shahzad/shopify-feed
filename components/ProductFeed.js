import React from "react";
import { useTable } from "react-table";
import { Button } from "./Button";

const range = (len) => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => {
  return {
    id: 11,
    name: "Haroon" + Math.random(),
    num: Math.floor(Math.random() * 30),
    status: Math.floor(Math.random() * 100),
    lastRefreshed: Math.floor(Math.random() * 100),
    feedUrl: "asdasdasd",
    viewFeed: "asdasd",
    editDelete: <Button>Button</Button>,
  };
};

function makeData(...lens) {
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth];
    return range(len).map((d) => {
      return {
        ...newPerson(),
      };
    });
  };

  return makeDataLevel();
}

function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  });

  // Render the UI for your table
  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

function App() {
  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Num",
        accessor: "num",
      },
      {
        Header: "Status",
        accessor: "status",
      },
      {
        Header: "Last Refreshed At",
        accessor: "lastRefreshed",
      },
      {
        Header: "Feed Url",
        accessor: "feedUrl",
      },
      {
        Header: "View Feed",
        accessor: "viewFeed",
      },
      {
        Header: "editDelete",
        accessor: "editDelete",
      },
    ],
    []
  );

  const data = React.useMemo(() => makeData(1), []);

  return (
    <div className="data-table">
      <h1>Products Feed</h1>
      <Table columns={columns} data={data} />
    </div>
  );
}

export default App;
